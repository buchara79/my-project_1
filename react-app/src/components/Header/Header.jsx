import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "./header.css";

function Header() {
  const countCart = useSelector((store) => store.cart.products.length);
  const countFavorits = useSelector ((store) => store.favorite.products.length)
  console.log(countCart);
  console.log(countFavorits);
  return (
    <header className="header">
      <div className="header-wraper">
        <Link to={"/"} className="header__logo">
          <img className="header__logo_img" src="logo.png" alt="Логотип" />
          <span>
            <span className="header__title_red">Мой</span>Маркет
          </span>
        </Link>
        <button className="header-button">
          <img
            className="header-button__img"
            src="./images/heder-heart.svg"
            alt=""
          />
          {countFavorits > 0 ? (
              <div className="header-button__counter favorits">{countFavorits}</div>
            ) : (
              ""
            )}
          <div>
            <img
              className="header-button__img"
              src="./images/heder-basket.svg"
              alt=""
            />
            {countCart > 0 ? (
              <div className="header-button__counter cart">{countCart}</div>
            ) : (
              ""
            )}
          </div>
        </button>
      </div>
    </header>
  );
}

export default Header;
