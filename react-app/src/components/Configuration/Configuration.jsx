import React from "react";
import cx from "classnames";
import { useState } from "react";

import "./configuration.css";

function Configuration(props) {
  const { memory } = props;
  const [select, setSelect] = useState("");
  return (
    <>
      <section className="configuration">
        <span className="configuration__title">
          Конфигурация памяти: {select}
        </span>
        <div className="configuration__wraper">
          {memory.map((config) => (
            <button
              key={config}
              className={cx("configuration__btn ", {
                configuration__btn_select: config === select,
              })}
              onClick={() => {
                setSelect(config);
              }}
            >
              {config}
            </button>
          ))}
        </div>
      </section>
    </>
  );
}
export default Configuration;
