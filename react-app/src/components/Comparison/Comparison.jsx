import React from "react";
import "./comparison.css";

function Comparison() {
  return (
    <>
      <section className="comparison">
        <span className="comparison__name">Сравнение моделей</span>
        <table className="comparison-table">
          <thead className="comparison-table__thead">
            <tr>
              <th className="comparison-table__cell">Модель</th>
              <th className="comparison-table__cell">Вес</th>
              <th className="comparison-table__cell">Высота</th>
              <th className="comparison-table__cell">Ширина</th>
              <th className="comparison-table__cell">Толщина</th>
              <th className="comparison-table__cell">Чип</th>
              <th className="comparison-table__cell">Объем памяти</th>
              <th className="comparison-table__cell">Аккумулятор</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="comparison-table__cell">iPhone 11</td>
              <td className="comparison-table__cell">194 грамма</td>
              <td className="comparison-table__cell">150.9 мм</td>
              <td className="comparison-table__cell">75.7 мм</td>
              <td className="comparison-table__cell">8.3 мм</td>
              <td className="comparison-table__cell">A13 Bionoc chip</td>
              <td className="comparison-table__cell">До 128 Гб</td>
              <td className="comparison-table__cell">До 17 часов</td>
            </tr>
            <tr>
              <td className="comparison-table__cell">iPhone 12</td>
              <td className="comparison-table__cell">164 грамма</td>
              <td className="comparison-table__cell">146.7 мм</td>
              <td className="comparison-table__cell">71.5 мм</td>
              <td className="comparison-table__cell">7.4 мм</td>
              <td className="comparison-table__cell">A14 Bionoc chip</td>
              <td className="comparison-table__cell">До256 Гб</td>
              <td className="comparison-table__cell">До 19 часов</td>
            </tr>
            <tr>
              <td className="comparison-table__cell">iPhone 13</td>
              <td className="comparison-table__cell">174 грамма</td>
              <td className="comparison-table__cell">146.7 мм</td>
              <td className="comparison-table__cell">71.5 мм</td>
              <td className="comparison-table__cell">7.65 мм</td>
              <td className="comparison-table__cell">A15 Bionoc chip</td>
              <td className="comparison-table__cell">До 512 Гб</td>
              <td className="comparison-table__cell">До 19 часов</td>
            </tr>
          </tbody>
        </table>
      </section>
    </>
  );
}
export default Comparison;
