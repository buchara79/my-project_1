import React from "react";

import "./AddProductButton.css";

function AddProductButton() {
  return (
    <div className="price__inbasket-wraper">
      <button className="price__inbasket_carront">
        <img
          className="price__inbasket-img"
          src="./images/icon-basket.svg"
          alt=""
        />
        Добавить в корзину
      </button>
    </div>
  );
}

export default AddProductButton;
