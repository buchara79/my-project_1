import React from "react";

import "./RemoveProductButton.css";

function RemoveProductButton() {
  return (
    <div className="price__inbasket-wraper">
      <button className="price__inbasket_grey">
        <img
          className="price__inbasket-img"
          src="./images/icon-basket.svg"
          alt=""
        />
        Товар уже в корзине
      </button>
    </div>
  );
}

export default RemoveProductButton;
