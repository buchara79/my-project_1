import React from "react";

import "./link.css";

function Link(props) {
  const { path, children } = props;
  return (
    <a className="link" href={`${path}`}>
      {children}
    </a>
  );
}
export default Link;
