import React from "react";
import cx from "classnames";
import { useState } from "react";

import "./color.css";

function Color(props) {
  const { colors } = props;
  const [select, setSelect] = useState("");

  return (
    <>
      <section className="color">
        <span className="color__title">Цвет товара: {select}</span>
        <div className="color__wraper">
          {colors.map((color, src) => (
            <button
              key={color.color}
              className={cx("color__btn", {
                color__btn_select: color.color === select,
              })}
              onClick={() => {
                setSelect(color.color);
              }}
            >              
              <img src={color.src} alt={color.alt}/>
            </button>
          ))}
        </div>
      </section>
    </>
  );
}
export default Color;
