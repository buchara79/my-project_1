import React from "react";
import { useCurrentDate } from "@kundinos/react-hooks";

import "./footer.css";

function Footer() {
  const currentDate = useCurrentDate();
  const fullYear = currentDate.getFullYear();
  return (
    <footer className="footer">
      <div className="footer-wraper">
        <div className="footer__copyright">
          <span className="footer__copyright_bold">
            © ООО «<span className="footer__copyright_red">Мой</span>Маркет»,
            <p>{`2018-${fullYear}.`}</p>
          </span>
          <span>
            Для уточнения информации звоните по номеру{" "}
            <a className="footer__link" href="tel:+7 900 000 0000">
              +7 900 000 0000
            </a>{" "}
            ,
          </span>
          <span>
            {" "}
            а предложения по сотрудничеству отправляйте на почту{" "}
            <a className="footer__link" href="mailto:partner@mymarket.com">
              partner@mymarket.com
            </a>
          </span>
        </div>
        <div>
          <a className="footer__link" href="#home">
            Наверх
          </a>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
