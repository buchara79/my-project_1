import './characteristics.css';

function Characteristics() {
    return <>
    <section className="characteristics">
                <span className="characteristics__title">
                  Характеристики товара
                </span>
                <div className="characteristics__list">
                  <div>
                    Экран: <span>6.1</span>
                  </div>
                  <div>
                    Встроенная память:{" "}
                    <span className="characteristics__list_bold">128 ГБ</span>
                  </div>
                  <div>
                    Операционная система:{" "}
                    <span className="characteristics__list_bold">iOS 15</span>
                  </div>
                  <div>
                    Беспроводные интерфейсы:{" "}
                    <span className="characteristics__list_bold">
                      NFC, Bluetooth, Wi-Fi
                    </span>
                  </div>
                  <div>
                    Процессор:
                    <a  rel="noopener noreferrer"
                      className="characteristics__link"
                      href="https://ru.wikipedia.org/wiki/Apple_A15"
                      target="_blank"
                    >
                      <b>Apple A15 Bionic</b>
                    </a>
                  </div>
                  <div>
                    Вес:
                    <span className="characteristics__list_bold">173 г</span>
                  </div>
                </div>
              </section>
    
    </>
}export default Characteristics;