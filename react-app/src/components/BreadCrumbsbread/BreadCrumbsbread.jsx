import React from "react";

import Link from "../Link/Link";
import "./breadCrumbsbread.css";

function BreadCrumbsbread(props) {
  const { items } = props;
  return (
    <>
      <div className="bread-crumbs">
        <nav>
          {items.map((items) => (
            <Link key={items.name} href={items.path}>
              {items.name}
            </Link>
          ))}
        </nav>
      </div>
    </>
  );
}

export default BreadCrumbsbread;
