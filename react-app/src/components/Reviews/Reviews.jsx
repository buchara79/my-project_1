import React from "react";

import "./reviews.css";

function Reviews(props) {
  const { reviews } = props;

  return (
    <>
      <section className="reviews">
        <div className="reviews__header">
          <span className="reviews__title">Отзывы</span>
          <span className="reviews__amount">425</span>
        </div>
        <div className="review-list">
          {reviews.map((review) => (
            <div key={review.id} className="review">
              <div className="review__wraper">
                <img className="review__foto" src={review.src} alt="" />
                <div className="review__content">
                  <div className="review__header">
                    <span className="review__nane">{review.name}</span>
                    <div>
                      {review.rating.map((star, index) => (
                        <img
                          className="review__img"
                          src={
                            star
                              ? "./images/star.png"
                              : "./images/grey-star.png"
                          }
                          alt="оценка"
                          key={index}
                        />
                      ))}
                    </div>
                  </div>
                  <div className="review__text">
                    <p>
                      <b>Опыт использования:</b> {review.experience}
                    </p>
                    <p>
                      <b>Достоинства:</b>
                      <br /> {review.advantages}
                    </p>
                    <p>
                      <b>Недостатки:</b>
                      <br /> {review.flaws}
                    </p>
                    <br />
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>
    </>
  );
}
export default Reviews;
