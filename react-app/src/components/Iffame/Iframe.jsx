import React from 'react'

function Iframe() {
  return (
    <div><iframe
    className="advertising__iframe"
    src="./banner.html"
    title="myFrame"
  >
    Ваш браузер не поддерживает фреймы!
  </iframe></div>
  )
}

export default Iframe