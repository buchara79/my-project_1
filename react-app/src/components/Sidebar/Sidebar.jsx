import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { addProduct, removeProduct } from "../../redusers/cart-reducer";
import { addFavorite, removeFavorite } from "../../redusers/favorite-reducer";
import { ReactComponent as Heart } from "../../icon/heart.svg";
import { ReactComponent as HeartRed } from "../../icon/heartRed.svg";

import AddProductButton from "../AddProductButton/AddProductButton";
import RemoveProductButton from "../RemoveProductButton/RemoveProductButton";
import Iframe from "../Iffame/Iframe";
import "./sidebar.css";

function Sidebare(props) {
  const { product } = props;
  const products = useSelector((store) => store.cart.products);
  const favorits = useSelector ((store) => store.favorite.products)
  const dispatch = useDispatch();
  const hasInCart = products.some((prevProduct) => {
    return prevProduct.id === product.id;
  });
  const hasInFavorite = favorits.some((prevProduct) => {
    return prevProduct.id === product.id;
  });

  function handleAddCart(e, product) {
    dispatch(addProduct(product));
  }
  function handleRemoveCart(e, product) {
    dispatch(removeProduct(product));
  }
  function handleAddFavorite(e, product) {
    dispatch(addFavorite(product));
  }
  function handleRemoveFavorite(e, product) {
    dispatch(removeFavorite(product));
  }

  return (
    <>
      <div className="price">
        <div className="price__price-old-wraper">
          <div className="price__price-old">
            <span className="price__price-old_crossed-out">75 900₽</span>
            <div className="price__discount">-8%</div>
          </div>
          <div>
            {hasInFavorite ? (
              <div onClick={(e) => handleRemoveFavorite(e, product)}>
                <HeartRed />
              </div>
            ) : (
              <div onClick={(e) => handleAddFavorite(e, product)}>
                <Heart className="price__icon-heart" />
              </div>
            )}
          </div>
        </div>
        <div className="price__price-new">
          <span className="price__price-new_bold"> 67 990₽</span>
        </div>
        <div className="price__delivery">
          <span>
            Самовывоз в четверг, 1 сентября —{" "}
            <span className="price__delivery_bold">бесплатно</span>
          </span>
          <span>
            Курьером в четверг, 1 сентября —{" "}
            <span className="price__delivery_bold">бесплатно</span>
          </span>
        </div>
        <div>
          {hasInCart ? (
            <div onClick={(e) => handleRemoveCart(e, product)}>
              <RemoveProductButton />
            </div>
          ) : (
            <div onClick={(e) => handleAddCart(e, product)}>
              <AddProductButton />
            </div>
          )}
        </div>
      </div>
      <div className="advertising">
        <span className="advertising__title">Реклама</span>
        <div>
          <Iframe />
        </div>
        <div>
          <Iframe />
        </div>
      </div>
    </>
  );
}
export default Sidebare;
