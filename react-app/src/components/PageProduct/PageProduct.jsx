import React from "react";
import { products } from "../../data";

import "./pageproduct.css";
import Header from "../Header/Header";
import BreadCrumbsbread from "../BreadCrumbsbread/BreadCrumbsbread";
import Color from "../Color/Color";
import Configuration from "../Configuration/Configuration";
import Characteristics from "../Characteristics/Characteristics";
import Description from "../Description/Description";
import Comparison from "../Comparison/Comparison";
import Sidebar from "../Sidebar/Sidebar";
import Reviews from "../Reviews/Reviews";
import Form from "../Form/Form";
import Footer from "../Footer/Footer";

function PageProduct() {
  return (
    <>
      <Header />
      <main className="main" id="home">
        <BreadCrumbsbread items={products.items} />
        <section className="product">
          <div className="product-name">
            <span className="product-name__name">
              Смартфон Apple iPhone 13, синий
            </span>
          </div>
          <div className="product-foto">
            <img
              className="product-foto__img"
              src="./images/image-1.svg"
              alt="Apple iPhone 13"
            />
            <img
              className="product-foto__img"
              src="./images/image-2.svg"
              alt="Apple iPhone 13"
            />
            <img
              className="product-foto__img"
              src="./images/image-3.svg"
              alt="Apple iPhone 13"
            />
            <img
              className="product-foto__img"
              src="./images/image-4.svg"
              alt="Apple iPhone 13"
            />
            <img
              className="product-foto__img"
              src="./images/image-5.svg"
              alt="Apple iPhone 13"
            />
          </div>
        </section>
        <section className="product-wraper">
          <div className="product-page">
            <Color colors={products.colors} />
            <Configuration memory={products.configuration} />
            <Characteristics />
            <Description />
            <Comparison />
          </div>
          <aside className="sidebar">
            <Sidebar product={products.product} />
          </aside>
        </section>
        <Reviews reviews={products.reviews} />
        <section className="form">
          <Form />
        </section>
      </main>
      <Footer />
    </>
  );
}

export default PageProduct;
