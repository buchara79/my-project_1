import React from "react";
import { useState } from "react";
import "./form.css";

function Form() {
  const [name, setName] = useState(localStorage.getItem("name") || "");
  const [grade, setGrade] = useState(localStorage.getItem("grade") || "");
  const [errorName, setErrorName] = useState("");
  const [errorGrade, setErrorGrade] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name.length === 0) {
      setErrorName("Вы забыли указать имя и фамилию");
      return;
    }
    if (name.length < 2) {
      setErrorName("Имя не может быть короче 2-хсимволов");
      return;
    }

    if (grade < 1 || grade > 5) {
      setErrorGrade("Оценка должна быть от 1 до 5");
      return;
    }
    alert("Ваш отзыв был успешно отправлен и будет отображён после модерации");
    localStorage.removeItem("name");
    localStorage.removeItem("grade");
    setName("");
    setGrade("");
  };
  const handleInputName = (e) => {
    setName(e.target.value);
    localStorage.setItem("name", e.target.value);
  };
  const handleInputGrade = (e) => {
    setGrade(e.target.value);
    localStorage.setItem("grade", e.target.value);
  };

  return (
    <>
      <form className="review-form" onSubmit={handleSubmit}>
        <legend className="review-form__title">Добавить свой отзыв</legend>
        <div className="eview-form__input">
          <div className="review-form__first-line">
            <div className="input-name">
              <input
                onChange={(e) => handleInputName(e)}
                onClick={() => setErrorName("")}
                className="review-form__name"
                value={name}
                type="text"
                name="name"
                id="name"
                placeholder="Имя и фамилия"
              />
              {errorName && <div className="error">{errorName}</div>}
            </div>
            <div className="input-grade">
              <input
                onChange={(e) => handleInputGrade(e)}
                onClick={() => setErrorGrade("")}
                className="review-form__grade"
                value={grade}
                type="number"
                name="grade"
                id="grade"
                placeholder="Оценка"
              />
              {errorGrade && <div className="error">{errorGrade}</div>}
            </div>
          </div>
          <textarea
            className="review-form__text"
            placeholder="Текст отзыва"
          ></textarea>
          <button className="review-form__button" type="submit">
            Отправить
          </button>
        </div>
      </form>
    </>
  );
}
export default Form;
