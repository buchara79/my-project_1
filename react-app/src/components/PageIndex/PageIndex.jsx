import React from "react";
import { Link } from "react-router-dom";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import "./pageIndex.css";

function PageIndex() {
  return (
    <div>
      <Header />
      <div className="pi-wrapper">
        <div className="pi-div">
          <div>
            <p>
              Здесь должно быть содержимое главной страницы. Но в рамках курса
              главная страница используется лишь в демонстрационных целях
            </p>
          </div>
          <Link className="link" to="/product">Перейти на страницу товара</Link>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default PageIndex;
