import { createSlice } from "@reduxjs/toolkit";


export const favoriteSlice = createSlice({
  name: "favorite",
  initialState: {
    products: [],
  },
  reducers: {
    addFavorite: (prevState, action) => {
      const products = action.payload;
      const hasInFavorite = prevState.products.some(
        (prevState) => prevState.id === products.id
      );
      if (hasInFavorite) return prevState;      
      return {
        ...prevState,
        products: [...prevState.products, action.payload],
      };
    },
    removeFavorite: (prevState, action) => {
      const products = action.payload;

      return {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== products.id;
        }),
      };
    },
  },
});
export const { addFavorite, removeFavorite } = favoriteSlice.actions;
export default favoriteSlice.reducer;