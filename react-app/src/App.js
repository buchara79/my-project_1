import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import PageIndex from "./components/PageIndex/PageIndex";
import PageProduct from "./components/PageProduct/PageProduct";
import PageNotFound from "./components/PageNotFound/PageNotFound";

// import PageProduct from "./components/PageProduct/PageProduct";
// import Header from "./components/Header/Header";
// import Footer from "./components/Footer/Footer";

function App() {
  return (
    // Эти два компонента обязательно нужно добавить,
    // чтобы роутинг в приложении работал корректно
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PageIndex />} />
        {/* Параметр path указываем по какому адресубудут доступна эта страница */}
        <Route path="/product" element={<PageProduct />} />
        {/* Если человек ввел другой адрес, то показываемстраницу 404 */}
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;