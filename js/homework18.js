"use strict"


// Упражнение 1
let a='100px';
let b='323px';// Выводим в консоль, должно получится 423
// Решение
let result=(parseInt(a) + parseInt(b))
console.log(result);

// Упражнение 2
console.log( Math.max(10, -45, 102, 36, 12, 0, -1));

// Упражнение 3

let c=123.3399;// Округлить до 123
// Решение
console.log(Math.round(c));

let d=0.111; // Округлить до 1
// Решение
console.log(Math.ceil(d));

let e=45.333333;// Округлить до 45.3
// Решение
console.log(e.toFixed(1));

let f=3;// Возвести в степень 5 (должно быть 243)
// Решение
f=3**5
console.log(f);

let g=400000000000000;// Записать в сокращенномвиде
// Решение
g=4e14
console.log(g);

let h ='1'!= 1;// Поправить условие, чтобы результат был true (значения изменять нельзя, только оператор)
// Решение
h ='1'!== 1
console.log(h);

// Упражнение 4
console.log(0.1+0.2===0.3);// Вернёт false, почему?
// Решение
let n = 0.1;
let m = 0.2;
let v = n + m; // Не равно 0.3
console.log(v.toFixed(20));