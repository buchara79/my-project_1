"use strict"

//Упражнение 1

let obj = {};

function isEmpty(obj) {
	for (let key in obj) {
		return false;
	}
	return true;
}

console.log(isEmpty(obj));


// Упражнение 3


/**
 * @param {object} salaries список зарплат до повышения.
 */

let salaries = {
	John: 100000,
	Ann: 160000,
	Pete: 130000,
};


/**
 * Функция,которая производит повышение зарплаты на определенный процент.
 * @param {number} perzent процент на который нужно увеличить зарплаты.
 * @return {object} возвращет объект с новыми зарплатами.
 */



function increaseSalary(perzent) {
	let newSalaries = {};   // @
	for (let key in salaries) {
		newSalaries[key] = Math.floor(salaries[key] + ((salaries[key] * perzent) / 100));
	}
	return newSalaries;
}

/**
 * Функция считает суммарное значение всех зарплат послеизменения.
 * @param {object} obj список зарплат сотрудников.
 * @returns {number} summ сумма всех зарплат.
 */

function calcSumm(obj) {
	let summ = 0;
	for (let key in obj) {
		summ += obj[key];
	}
	return summ;
}

let result = increaseSalary(5);
let summ = calcSumm(result);
console.log(result, summ);

