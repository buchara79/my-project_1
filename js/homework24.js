"use strict"

let form = document.querySelector('.review-form');
let inputCont = form.querySelector('.review-form__first-line');
let btn = form.querySelector('.review-form__button');
let inputNameCont = inputCont.querySelector('.input-name');
let inputName = inputNameCont.querySelector('.review-form__name');
let inputGradeCont = inputCont.querySelector('.input-grade');
let inputGrade = inputGradeCont.querySelector('.review-form__grade');

let errDiv = document.createElement('div');
errDiv.classList.add('error');
let errDivText = document.createTextNode('');

let t;
let d;

form.addEventListener('submit', (e) => {
	e.preventDefault();
	if (inputName.value.length == 0) {
		errDiv.innerHTML = 'Вы забыли указать имя и фамилию';
		inputNameCont.appendChild(errDiv);
		t = 1;
		return;
	} else
		if (inputName.value.length < 2) {
			errDiv.innerHTML = 'Имя не может быть короче 2-хсимволов';
			inputNameCont.appendChild(errDiv)
			t = 1;
			return;
		} else
			if (inputGrade.value < 1 || inputGrade.value > 5) {
				console.log(inputGrade.value);
				errDiv.innerHTML = 'Оценка должна быть от 1 до 5';
				inputGradeCont.appendChild(errDiv);
				d = 1;
				return;
			}
			form.submit();			
});

inputName.addEventListener('focus', () => {
	if (t != 1) return;
	else
	inputNameCont.removeChild(errDiv);
	t = 0;
})

inputGrade.addEventListener('focus', () => {
	if (d != 1) return;
	else 
	inputGradeCont.removeChild(errDiv);
	d = 0;
})