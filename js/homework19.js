"use strict"

// Упражнение 1
let a='$100';
let b='300$';
let summ = +a.slice(1) + +parseInt(b);// Ваше решение
console.log(summ);// Должно быть 400


// Упражнение 2
let message=' привет, медвед      ';
message = (message.trim())
message = message[0].toUpperCase() + message.slice(1);// Решение должно быть написано тут
console.log(message);// “Привет, медвед”


// Упражнение 3
let age = +prompt('Сколько вам лет?');
if (age >= 0 && age <= 3) {alert(`Вам ${age} лет и вы младенец`)}
else if (age >= 4 && age <= 11){alert(`Вам ${age} лет и вы ребенок`)}
else if (age >= 12 && age <= 18){alert(`Вам ${age} лет и вы подросток`)}
else if (age >= 19 && age <= 40){alert(`Вам ${age} лет и вы познаёте жизнь`)}
else if (age >= 41 && age <= 80){alert(`Вам ${age} лет и вы познали жизнь`)}
else if (age >= 81 ){alert(`Вам ${age} лет и вы долгожитель`)}
else alert(`Не корректное значение`)


// Упражнение 4

let messag ='Я работаю со строками как профессионал!';
let count = messag.split(/\s+/);//Ваше решение
console.log(count.length); //Должно быть 6