"use strict"

// Упражнение 1
let arr = [1, 2, `b`, 10, {}, 5];
let summ = 0;
arr.forEach(getSumm);
function getSumm(item) {
	if (Number.isFinite(item)) { // Проверяем, является ли число
		summ = summ + item;			  // Если число, складываем 
	}
	return summ;
};
console.log(getSumm(arr));


// Упражнение 3

let cart = [4884];

// Добавить товар
function addToCart(productId) {
	let hasInCart = cart.includes(productId)
	if (hasInCart) return;
	cart.push(productId);
}
// Удалить товар
function removeFromCart(productId) {
	cart = cart.filter(function (id) {
		return id !== productId;
	})
}


addToCart(4567);
// Добавит повторно
addToCart(4567);

addToCart(4587);

removeFromCart(4884);


console.log(cart)
