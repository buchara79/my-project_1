"use strict";

let form = document.querySelector(".review-form");
let inputCont = form.querySelector(".review-form__first-line");
let btn = form.querySelector(".review-form__button");
let inputNameCont = inputCont.querySelector(".input-name");
let inputName = inputNameCont.querySelector(".review-form__name");
let inputGradeCont = inputCont.querySelector(".input-grade");
let inputGrade = inputGradeCont.querySelector(".review-form__grade");

let errDiv = document.createElement("div");
errDiv.classList.add("error");
let errDivText = document.createTextNode("");

let btnHeader = document.querySelector(".header-button");
let btnInBasket = document.querySelector(".price__inbasket");
let btnHeaderCounter = document.createElement("div");
btnHeaderCounter.classList.add("header-button__counter");

let iconHeard = document.querySelector(".price__icon-heart");

iconHeard.addEventListener("click", () => {
  iconHeard.style.color = "#F36223";
});

if (localStorage.counter) {
  addProduct();
}
btnInBasket.addEventListener("click", () => {
  if (!localStorage.counter) {
    addProduct();
    localStorage.setItem("counter", 1);
   
  } else if (localStorage.counter) {
    removeProduct();
    delete localStorage.counter;
  }
});

function addProduct() {
  btnHeader.appendChild(btnHeaderCounter);
  btnHeaderCounter.innerHTML = "1";
  btnInBasket.innerHTML = "Товар уже в корзине";
  btnInBasket.style.background = "#888888";
}

function removeProduct() {
  btnHeader.removeChild(btnHeaderCounter);
  btnInBasket.innerHTML = "Добавить в корзину";
  btnInBasket.style.background = "";
}

let flagErrName = false;
let flagErrGrade = false;

form.addEventListener("submit", (e) => {
  e.preventDefault();
  if (inputName.value.length == 0) {
    errDiv.innerHTML = "Вы забыли указать имя и фамилию";
    inputNameCont.appendChild(errDiv);
    flagErrName = true;
    return;
  } else if (inputName.value.length < 2) {
    errDiv.innerHTML = "Имя не может быть короче 2-хсимволов";
    inputNameCont.appendChild(errDiv);
    flagErrName = true;
    return;
  } else if (inputGrade.value < 1 || inputGrade.value > 5) {
    console.log(inputGrade.value);
    errDiv.innerHTML = "Оценка должна быть от 1 до 5";
    inputGradeCont.appendChild(errDiv);
    flagErrGrade = true;
    return;
  }
  form.submit();
  delete localStorage.name;
  delete localStorage.grade;
});

inputName.addEventListener("focus", () => {
  if (!flagErrName) return;
  else inputNameCont.removeChild(errDiv);
  flagErrName = false;
});

inputGrade.addEventListener("focus", () => {
  if (!flagErrGrade) return;
  else inputGradeCont.removeChild(errDiv);
  flagErrGrade = false;
});

inputName.addEventListener("input", (event) => {
  let value = event.target.value;
  let name = event.target.getAttribute("name");
  localStorage.setItem(name, value);
});

inputGrade.addEventListener("input", (event) => {
  let value = event.target.value;
  let name = event.target.getAttribute("name");
  localStorage.setItem(name, value);
});

inputName.value = localStorage.getItem("name");
inputGrade.value = localStorage.getItem("grade");






