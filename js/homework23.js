"use strict"

// Упражнение 1

let count = +prompt('Введите число');
if (Number.isFinite(count)) {
	let intervalId = setInterval(() => {
		console.log(`Осталось: ${count}`);
		count = count - 1;
		if (count === 0) {
			console.log('Ваше время вышло');
			clearInterval(intervalId);
		}
	}, 1000);
} else {
	alert('Вы ввели не число');
}


// Упражнение 2


let promise = fetch('https://reqres.in/api/users');
promise
	.then((response) => {
		return response.json();
	})
	.then((response) => {
		let users = response.data;
		let message = '';
		message += `Получили пользователей: ${users.length} \n`;
		users.forEach((user) => {
			message += `-${user.first_name} ${user.last_name} (${user.email}) \n`;
		})
		console.log(message);
	})
	.catch(() => {
		console.log('Обещание нарушено ')
	})



